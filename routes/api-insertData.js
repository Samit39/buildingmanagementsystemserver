module.exports = function(app) {
    app.post("/postData", function(req, res) {
        console.log("Hello from postData");
        /*
        //OLD
        currentCollection.insertOne(req.body, function(err, result) {
            // function(err,result){
            if (err) {
                console.log(err);
            } else {
                console.log("Data Posted", req.body);
                res.send(req.body);
            }
            // }
        });
        // console.log("Data Posted", req.body);
        // res.send(req.body);
        */
        // console.log(req.body);
        // console.log("JSONPARSED", JSON.parse(req.body.allLogsData));
        currentCollection.insertMany(JSON.parse(req.body.allLogsData), function(err, result) {
            if (err) {
                console.log(err);
            } else {
                console.log("Data Posted to MongoDB Success", req.body);
                res.send({ status: "Success", returnedData: req.body.allLogsData });
            }
        });
        console.log("Data Posted", req.body);
    });
}