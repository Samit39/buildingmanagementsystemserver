module.exports = function(app) {
    const fs = require('fs'); //Require file system

    app.post("/writetofile", function(req, res) {
        if (!req.body) {
            return res.sendStatus(400);
        } else {
            console.log("writing to file");
            fs.appendFile('logs/log.dat', '\r\n' + req.body.allLogsData, function(err, data) {
                res.send({ status: "Success", returnedData: req.body.allLogsData });
            });
        }
    });
}