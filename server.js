const express = require('express'); //Rewuire express
const cors = require('cors'); //Require Cors
const bodyParser = require('body-parser'); //Require body parser
const app = express(); //Use Express

require('./config.js')(app);

app.use(cors()); //Use cors
app.use(bodyParser.json()); //use body parser
app.use(bodyParser.urlencoded({ extended: true }));

var currentCollection; //initialize variable

const http = require('http').Server(app); //require http

http.listen(3000, () => {
    console.log("Server started at port 3000");
});

//Base Route
require('./routes/homepage.js')(app);

//Route to get Cloud data
require('./routes/api-getCloudData.js')(app);

//Route to insert data
require('./routes/api-insertData.js')(app);

//Route to write to local file
require('./routes/api-writeToFile.js')(app);

//From Here Starts The Building Management Routes
require('./routes/api-login.js')(app);